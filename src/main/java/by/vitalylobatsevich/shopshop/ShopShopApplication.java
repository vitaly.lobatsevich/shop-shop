package by.vitalylobatsevich.shopshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopShopApplication {
    public static void main(final String[] args) {
        SpringApplication.run(ShopShopApplication.class, args);
    }
}
